import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateInstanceComponent } from './interface/create-instance/create-instance.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntryComponent } from './interface/entry/entry.component';
import { OverviewComponent } from './interface/overview/overview.component';
import { GroupsService } from './services/groups.service';
import { GroupPageComponent } from './interface/group-page/group-page.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateInstanceComponent,
    EntryComponent,
    OverviewComponent,
    GroupPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [GroupsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
