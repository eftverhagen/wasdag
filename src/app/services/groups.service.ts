import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  public items;

  constructor() {
    this.items = [
      {name: "T-shirts"},
      {name: "Broeken"},
      {name: "Lapjes"},
    ];
  }

  addGroup(groupName) {
    this.items.push({name: groupName});
  }
}
