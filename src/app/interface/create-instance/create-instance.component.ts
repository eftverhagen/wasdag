import { Component, OnInit } from '@angular/core';
import { GroupsService } from 'src/app/services/groups.service';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-create-instance',
  templateUrl: './create-instance.component.html',
  styleUrls: ['./create-instance.component.scss']
})
export class CreateInstanceComponent implements OnInit {
  public title = "Group";
  public myGroup;

  constructor(public _groups: GroupsService) { 
    this.myGroup = new FormGroup({
      groupName: new FormControl('')
    });
  }

  onSubmit(event) {
    this._groups.addGroup(this.myGroup.get('groupName').value);
  }
  
  ngOnInit() {
  }

}
