import { GroupsService } from './../../services/groups.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.scss']
})
export class GroupPageComponent implements OnInit {
  public groupName;

  constructor(public _groups: GroupsService) { 
    this.groupName = this._groups.items[0].name;
  }

  ngOnInit() {
  }

}
