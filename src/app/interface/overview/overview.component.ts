import { Component, OnInit } from '@angular/core';
import { GroupsService } from 'src/app/services/groups.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  public groups;

  constructor(public _groups: GroupsService) { 
    this.groups = this._groups.items;
  }

  ngOnInit() {
  }

}
